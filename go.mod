module github.com/longdahai/cmd

go 1.12

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20190515213511-eb9f6a1743f3
	github.com/go-sql-driver/mysql v1.4.1
	github.com/go-xorm/xorm v0.7.3-0.20190617053813-c9b14f948700
	github.com/lib/pq v1.1.1
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/ziutek/mymysql v1.5.4
	xorm.io/core v0.7.1-0.20190906123629-0f412da7e997
)
